const { KohanaJS } = require('kohanajs');

module.exports = {
  templateFolder: `${KohanaJS.EXE_PATH}/../public/media/edm`,
  databasePath: `${KohanaJS.EXE_PATH}/../database`,
  cache: true,
  webhookKey: 'any-example-key',
};
