const { Controller } = require('@kohanajs/core-mvc');
const { ControllerMixinDatabase, KohanaJS, ORM } = require('kohanajs');

const Webhook = ORM.require('Webhook');

class ControllerWebhook extends Controller {
  static mixins = [...Controller.mixins, ControllerMixinDatabase];

  constructor(request) {
    super(request);

    this.state.get(ControllerMixinDatabase.DATABASE_MAP).set('webhook', KohanaJS.config.mail.dbWebhook);
  }

  async action_notification() {
    this.headers['Content-Type'] = 'application/json';

    const { service } = this.request.params;
    const { type } = this.request.params;
    const webhook = ORM.create(Webhook, { database: this.state.get('databases').get('webhook') });
    Object.assign(webhook, {
      service,
      type,
      value: ((typeof this.request.body) === 'object') ? JSON.stringify(this.request.body) : this.request.body,
    });

    if (service === 'mailgun') {
      const data = this.request.body;
      const event = data['event-data'];
      const userVariable = event['user-variables'];

      webhook.entity = 'message-id';
      webhook.entity_id = event.message.headers['message-id'];
      webhook.email = event.recipient;
      webhook.user_key = userVariable?.key || '';
      webhook.user_value = userVariable?.value || '';
    }

    await webhook.write();

    this.body = JSON.stringify({ success: true, payload: 'received' });
  }
}

module.exports = ControllerWebhook;
